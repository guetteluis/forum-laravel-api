<?php

namespace App\Models;

use App\Transformers\ActivitiesTransformer;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{

    protected $guarded = [];


    public function subject() {

        return $this->morphTo();

    }

    /**
     * @param User|\Illuminate\Contracts\Auth\Authenticatable $user
     * @return array
     */
    public static function feed(User $user) {

        $paginatedActivities = static::where('user_id', $user->id)
            ->latest()
            ->with('subject')
            ->paginate(10);

        return [
            'current_page' => $paginatedActivities->currentPage(),
            'last_page' => $paginatedActivities->lastPage(),
            'items' => (new ActivitiesTransformer())->transformCollection($paginatedActivities)
        ];

    }

}
