<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{

    use RecordsActivity;

    protected $guarded = [];

    protected $with = ['favorited'];

    public function favorited() {
        return $this->morphTo();
    }
}
