<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $appends = ['can_update'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar_path'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads() {
        return $this->hasMany(Thread::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities() {
        return $this->hasMany(Activity::class);
    }

    public function lastReply() {
        return $this->hasOne(Reply::class)->latest();
    }

    /**
     * @param Thread $thread
     */
    public function read(Thread $thread) {

        cache()->forever(
            $this->visitedThreadCacheKey($thread),
            Carbon::now()
        );

    }

    /**
     * @param Thread $thread
     * @return string
     */
    public function visitedThreadCacheKey(Thread $thread) {

        return sprintf("users.%s.visits.%s", $this->id, $thread->id);

    }

    public function getCanUpdateAttribute() {

        $user = auth('api')->user();

        return $user && $user->can('update', $this);

    }

}
