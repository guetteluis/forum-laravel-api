<?php
/**
 * Created by PhpStorm.
 * User: guetteluis
 * Date: 06/01/18
 * Time: 09:01 PM
 */

namespace App\Models;


trait Favoritable
{

    protected static function bootFavoritable() {

        static::deleting(function($model) {
            $model->favorites->each->delete();
        });

    }

    public function favorites(){
        return $this->morphMany(Favorite::class, 'favorited');
    }

    public function getFavoritesCountAttribute() {
        return $this->favorites->count();
    }

    public function getFavoritedAttribute() {
        return !! $this->favorites->where('user_id', auth()->guard('api')->id())->count();
    }

    public function favorite() {
        $attributes = ['user_id' => auth()->guard('api')->id()];

        if (!$this->favorites()->where($attributes)->exists()) {
            return $this->favorites()->create($attributes);
        }
    }

    public function unfavorite() {

        $attributes = ['user_id' => auth()->guard('api')->id()];

        return $this->favorites()->where($attributes)->get()->each->delete();

    }

}