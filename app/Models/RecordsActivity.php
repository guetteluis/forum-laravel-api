<?php
/**
 * Created by PhpStorm.
 * User: guetteluis
 * Date: 06/01/18
 * Time: 08:34 PM
 */

namespace App\Models;


trait RecordsActivity
{

    protected static function bootRecordsActivity() {

        if(auth()->guard('api')->guest()) return;

        foreach (static::recordEvents() as $event) {
            static::$event(function ($model) use ($event) {
                $model->recordActivity($event);
            });
        }

        static::deleting(function($model) {
            $model->activity->each->delete();
        });

    }

    protected static function recordEvents() {

        return ['created'];

    }

    /**
     * @param $event
     */
    protected function recordActivity($event) {

        $this->activity()->create([
            'user_id' => auth()->guard('api')->id(),
            'type' => $this->getActivityType($event),
        ]);

    }

    public function activity() {
        return $this->morphMany(Activity::class, 'subject');
    }

    /**
     * @param $event
     * @return string
     */
    protected function getActivityType($event)
    {
        $type = strtolower((new \ReflectionClass($this))->getShortName());

        return "{$event}_{$type}";
    }
}