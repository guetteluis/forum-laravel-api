<?php

namespace App\Models;

use App\Events\ThreadReceivedNewReply;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use RecordsActivity;

    protected $guarded = [];

    protected $with = ['creator:id,name', 'channel'];

    protected $appends = ['subscribed', 'has_updates', 'published_at', 'can_update'];

    protected static function boot() {

        Parent::boot();


        static::deleting(function ($thread) {
            $thread->replies->each->delete();
        });

    }

    /**
     * A thread has many replies
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies() {
        return $this->hasMany(Reply::class);
    }


    /**
     * A thread belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator() {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * A thread belongs to a channel
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel() {
        return $this->belongsTo(Channel::class);
    }

    /**
     * @param $query
     * @param $filters
     * @return mixed
     */
    public function scopeFilter($query, $filters) {
        return $filters->apply($query);
    }

    /**
     * @param $reply
     * @return Model
     */
    public function addReply($reply) {

        $reply = $this->replies()->create($reply);

        event(new ThreadReceivedNewReply($reply));

        return $reply;

    }

    /**
     * @param null $userId
     * @return $this
     */
    public function subscribe($userId = null) {

        $this->subscriptions()->create([
            'user_id' => $userId?:auth()->guard('api')->id()
        ]);

        return $this;

    }

    /**
     * @param null $userId
     * @return $this
     */
    public function unsubscribe($userId = null) {

        $this->subscriptions()
            ->where(['user_id' => $userId?:auth()->guard('api')->id()])
            ->delete();

        return $this;

    }

    public function subscriptions() {

        return $this->hasMany(ThreadSubscription::class);

    }

    /**
     * @return bool
     */
    public function getSubscribedAttribute() {

        return $this->subscriptions()
                ->where('user_id', auth()->guard('api')->id())
                ->count() > 0;

    }

    public function getHasUpdatesAttribute() {

        if (auth('api')->check()) {
            $key = auth()->guard('api')->user()->visitedThreadCacheKey($this);

            return $this->updated_at > cache($key);
        }

        return true;

    }

    public function getPublishedAtAttribute() {

        return $this->created_at?$this->created_at->diffForHumans():null;

    }

    public function getCanUpdateAttribute() {

        $user = auth('api')->user();

        return $user && $user->can('update', $this);

    }

}
