<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{

    use Favoritable, RecordsActivity;

    protected $guarded = [];

    protected $with = ['owner:id,name', 'thread'];

    protected $appends = ['mentioned_usernames'];

    protected static function boot()
    {
        parent::boot();

        static::created(function($reply) {
           $reply->thread->increment('replies_count');
        });

        static::deleted(function($reply) {
            $reply->thread->decrement('replies_count');
        });

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner() {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread() {
        return $this->belongsTo(Thread::class);
    }

    public function getMentionedUsersAttribute() {

        preg_match_all('/\@([\w\-]+)/', $this->body, $matches);

        return User::whereIn('name', $matches[1])->get();

    }

    public function getMentionedUsernamesAttribute() {

        return $this->getMentionedUsersAttribute()
            ->map(function ($user) {
                return $user->name;
            })->toArray();

    }

    public function wasJustPublished() {
        return $this->created_at->gt(Carbon::now()->subMinute());
    }

}
