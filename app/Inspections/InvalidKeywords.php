<?php
/**
 * Created by PhpStorm.
 * User: guetteluis
 * Date: 27/01/18
 * Time: 11:31 PM
 */

namespace App\Inspections;

use \Exception;

class InvalidKeywords
{

    protected $kewords = [
        'yahoo customer support'
    ];

    public function detect($body) {

        foreach ($this->kewords as $keyword) {

            if (stripos($body, $keyword) !== false) {
                throw new Exception('Your Reply contain spam');
            }

        }

    }

}