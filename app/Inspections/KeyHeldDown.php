<?php
/**
 * Created by PhpStorm.
 * User: guetteluis
 * Date: 27/01/18
 * Time: 11:37 PM
 */

namespace App\Inspections;

use Exception;

class KeyHeldDown
{

    public function detect($body) {

        if (preg_match('/(.)\\1{4,}/', $body)) {
            throw new Exception('Your Reply contain spam');
        }

    }

}