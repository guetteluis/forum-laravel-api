<?php

namespace App\Http\Forms;

use App\Exceptions\ThrottleException;
use App\Models\Reply;
use App\Rules\SpamFree;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class EditPostForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     * @throws ThrottleException
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => ['required', new SpamFree]
        ];
    }
}
