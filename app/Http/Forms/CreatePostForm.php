<?php

namespace App\Http\Forms;

use App\Exceptions\ThrottleException;
use App\Models\Reply;
use App\Rules\SpamFree;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class CreatePostForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     * @throws ThrottleException
     */
    public function authorize()
    {
        if (Gate::denies('create', new Reply)) {
            throw new ThrottleException();
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => ['required', new SpamFree]
        ];
    }

    /**
     * @param $thread
     */
    public function persist($thread) {

        return $thread->addReply([
            'body' => request('body'),
            'user_id' => auth()->id()
        ]);

    }
}
