<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\User;
use App\Transformers\BasicProfileTransformer;

class ProfilesController extends Controller
{

    /**
     *
     * @param User $user
     * @param BasicProfileTransformer $transformer
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, BasicProfileTransformer $transformer) {

        $user['since_at'] = $user->created_at->diffForHumans();
        $user['activities_data'] = Activity::feed($user);

        if (auth()->guard('api')->id() !== $user->id) {
            $user = $transformer->transform($user);
        }

        return response()->json([
           'profile' => $user
        ]);

    }

}
