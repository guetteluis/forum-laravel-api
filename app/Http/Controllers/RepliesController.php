<?php

namespace App\Http\Controllers;

use App\Http\Forms\CreatePostForm;
use App\Http\Forms\EditPostForm;
use App\Models\Reply;
use App\Models\Thread;
use App\Transformers\ReplyTransformer;

class RepliesController extends Controller
{
    /**
     * RepliesController constructor.
     */
    public function __construct() {
        $this->middleware('auth:api')->except(['index']);
    }

    public function index($channel, $thread) {

        $paginatedReplies = Reply::where('thread_id', $thread)
            ->latest()
            ->paginate(10);

        return response()->json([
            'replies_data' => [
                'current_page' => $paginatedReplies->currentPage(),
                'last_page' => $paginatedReplies->lastPage(),
                'items' => (new ReplyTransformer)->transformCollection($paginatedReplies)
            ]
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $channel
     * @param Thread $thread
     * @param CreatePostForm $form
     */
    public function store($channel, Thread $thread, CreatePostForm $form) {
        $form->persist($thread);
    }

    /**
     * @param Reply $reply
     * @param EditPostForm $form
     * @internal param Request $request
     */
    public function update(Reply $reply, EditPostForm $form) {

        $this->authorize('update', $reply);

        $reply->update($form->only(['body']));

    }

    /**
     * @param Reply $reply
     */
    public function destroy(Reply $reply) {

        $this->authorize('update', $reply);

        $reply->delete();
    }

}
