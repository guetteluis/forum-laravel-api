<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\User;
use App\Transformers\BasicProfileTransformer;
use Illuminate\Http\Request;

class UserAvatarController extends Controller
{

    /**
     * UserAvatarController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function store(Request $request) {

        $request->validate([
            'avatar' => 'required|image'
        ]);

        auth('api')->user()->update([
            'avatar_path' => $request->file('avatar')->store('avatars', 'public')
        ]);

    }

}
