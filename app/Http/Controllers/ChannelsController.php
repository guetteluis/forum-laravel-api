<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use Illuminate\Http\Request;

class ChannelsController extends Controller
{

    protected $channel;

    /**
     * ChannelsController constructor.
     * @param Channel $channel
     */
    public function __construct(Channel $channel) {

        $this->channel = $channel;

    }

    public function index() {

        return response()->json([
           'channels' => $this->channel->orderBy('name')->get()
        ]);

    }
}
