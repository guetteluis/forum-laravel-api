<?php

namespace App\Http\Controllers;

use App\Models\Favorite;
use App\Models\Reply;

class FavoritesController extends Controller
{
    /**
     * @var Favorite
     */
    protected $favorite;

    /**
     * FavoritesController constructor.
     * @param Favorite $favorite
     */
    public function __construct(Favorite $favorite) {

        $this->middleware('auth:api');
        $this->favorite = $favorite;

    }

    public function store(Reply $reply) {
        return $reply->favorite();
    }

    public function destroy(Reply $reply) {
        $reply->unfavorite();
    }

}
