<?php

namespace App\Http\Controllers;

use App\Filters\ThreadFilters;
use App\Http\Requests\ThreadRequest;
use App\Models\Channel;
use App\Models\Thread;
use App\Transformers\StoreThreadTransformer;

class ThreadsController extends Controller
{

    /**
     * ThreadsController constructor.
     */
    public function __construct() {
        $this->middleware('auth:api')->except(['index', 'show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Channel $channel
     * @param ThreadFilters $filters
     * @return \Illuminate\Http\Response
     * @internal param null $channelSlug
     */
    public function index(Channel $channel, ThreadFilters $filters) {

        $paginatedThreads = $this->getThreads($channel, $filters);

        return response()->json([
            'threads_data' => [
                'current_page' => $paginatedThreads->currentPage(),
                'last_page' => $paginatedThreads->lastPage(),
                'items' => $paginatedThreads->toArray()['data']
            ]
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ThreadRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ThreadRequest $request) {

        Thread::create([
            'user_id' => auth()->id(),
            'channel_id' => $request->get('channel_id'),
            'title' => $request->get('title'),
            'body' => $request->get('body')
        ]);

    }

    /**
     * @param $channel
     * @param $thread_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($channel, $thread_id) {

        $thread = Thread::find($thread_id);

        if (!$thread) return response()->json(['message' => 'Not Found'], 404);

        if (auth('api')->check()) {
            auth('api')->user()->read($thread);
        }

        return response()->json([
            'thread' => $thread
        ]);

    }

    public function destroy($channel, Thread $thread) {

        $this->authorize('update', $thread);

        $thread->delete();

        return response()->json([], 204);
    }

    /**
     * @param Channel $channel
     * @param ThreadFilters $filters
     * @return \Illuminate\Database\Query\Builder|static
     */
    protected function getThreads(Channel $channel, ThreadFilters $filters)
    {
        if ($channel->exists) {
            $threads = $channel->threads()->latest();
        } else {
            $threads = Thread::latest();
        }

        return $threads->filter($filters)->paginate(10);

    }
}
