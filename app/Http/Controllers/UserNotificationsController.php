<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserNotificationsController extends Controller
{


    /**
     * UserNotificationsController constructor.
     */
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function index($user) {

        return response()->json([
            'notifications' => auth()->guard('api')->user()->unreadNotifications
        ]);

    }

    public function destroy($user, $notificationId) {

        auth()->guard('api')->user()
            ->notifications()
            ->findOrFail($notificationId)
            ->delete();

    }

}
