<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use Illuminate\Http\Request;

class ThreadSubscriptionsController extends Controller
{


    /**
     * ThreadSubscriptionsController constructor.
     */
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function index() {

    }

    public function store($channelSlug, Thread $thread) {

        $thread->subscribe();

    }

    public function destroy($channelSlug, Thread $thread) {

        $thread->unsubscribe();

    }

}
