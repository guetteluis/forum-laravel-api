<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Transformers\BasicProfileTransformer;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * UsersController constructor.
     */
    public function __construct() {

        $this->middleware('auth:api')->except('index');

    }

    public function index() {

        $search = \request('name');

        $users = User::where('name', 'LIKE', "$search%")
            ->take(5)
            ->pluck('name');

        return response()->json([
            'users' => $users
        ]);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function authenticated() {

        return response()->json([
            'user' => auth()->user()
        ]);

    }
}
