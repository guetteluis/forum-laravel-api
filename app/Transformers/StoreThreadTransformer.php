<?php
/**
 * Created by PhpStorm.
 * User: guetteluis
 * Date: 01/01/18
 * Time: 08:46 PM
 */

namespace App\Transformers;


class StoreThreadTransformer extends Transformer {

    /**
     * @param $thread
     * @param array $embeds
     * @return mixed
     */
    public function transform($thread, $embeds = []) {

        $append['channel'] = $thread->channel;

        $thread = array_merge($append, $thread->toArray());

        return $thread;

    }

}