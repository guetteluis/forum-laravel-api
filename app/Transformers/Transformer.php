<?php

namespace App\Transformers;


abstract class Transformer {

    /**
     * @param $items
     * @param array $embeds
     * @return array
     */
    public function transformCollection($items, $embeds = []) {

        return $items->map(function($item) use($embeds) {
           return $this->transform($item, $embeds);
        });

    }

    /**
     * @param $item
     * @param array $embeds
     * @return mixed
     */
    public abstract function transform($item, $embeds = []);
}