<?php
/**
 * Created by PhpStorm.
 * User: guetteluis
 * Date: 01/01/18
 * Time: 08:46 PM
 */

namespace App\Transformers;


class ReplyTransformer extends Transformer {

    /**
     * @param $reply
     * @param array $embeds
     * @return mixed
     */
    public function transform($reply, $embeds = []) {

        $user = auth()->guard('api')->user();

        $reply['favorited'] = !! $reply->favorited;
        $reply['favorites_count'] = $reply->favorites_count;
        unset($reply['favorites']);

        $reply['posted_at'] = $reply->created_at->diffForHumans();

        $reply['can_update'] = $user && $user->can('update', $reply);

        return $reply;

    }

}