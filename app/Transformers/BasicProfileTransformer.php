<?php

namespace App\Transformers;


class BasicProfileTransformer extends Transformer {

    /**
     * @param $user
     * @param array $embeds
     * @return array
     */
    public function transform($user, $embeds = []) {

        unset($user['email']);
        unset($user['id']);

        return $user;

    }

}