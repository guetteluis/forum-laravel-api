<?php
/**
 * Created by PhpStorm.
 * User: guetteluis
 * Date: 01/01/18
 * Time: 08:46 PM
 */

namespace App\Transformers;


class ThreadTransformer extends Transformer {

    /**
     * @param $thread
     * @param array $embeds
     * @return mixed
     */
    public function transform($thread, $embeds = []) {

        $user = auth()->guard('api')->user();

        $thread['published_at'] = $thread['created_at']->diffForHumans();

        $thread['can_update'] = $user && $user->can('update', $thread);

        return $thread;

    }

}