<?php

namespace App\Transformers;


class ActivitiesTransformer extends Transformer {

    /**
     * @param $activity
     * @param array $embeds
     * @return mixed
     */
    public function transform($activity, $embeds = []) {

        $activity['made_at'] = $activity['created_at']->diffForHumans();

        return $activity;

    }

}