<?php

use Faker\Generator as Faker;

$factory->define(\Illuminate\Notifications\DatabaseNotification::class, function (Faker $faker) {

    return [
        'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
        'type' => 'App\\Notification\\ThreadWasUpdated',
        'notifiable_id' => function () {
            return auth('api')->id()?:factory(\App\Models\User::class)->create()->id;
        },
        'notifiable_type' => 'App\\Models\\User',
        'data' => ['foo' => 'bar']
    ];

});
