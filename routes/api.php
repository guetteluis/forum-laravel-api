<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('threads', 'ThreadsController@index');
Route::get('threads/{channel}', 'ThreadsController@index');
Route::post('threads', 'ThreadsController@store');
Route::get('threads/{channel}/{thread}', 'ThreadsController@show');
Route::delete('threads/{channel}/{thread}', 'ThreadsController@destroy');

Route::get('threads/{channel}/{thread}/replies', 'RepliesController@index');
Route::post('threads/{channel}/{thread}/replies', 'RepliesController@store');
Route::patch('replies/{reply}', 'RepliesController@update');
Route::delete('replies/{reply}', 'RepliesController@destroy');

//Route::get('threads/{channel}/{thread}/subscriptions', 'ThreadSubscriptionsController@index');
Route::post('threads/{channel}/{thread}/subscriptions', 'ThreadSubscriptionsController@store');
Route::delete('threads/{channel}/{thread}/subscriptions', 'ThreadSubscriptionsController@destroy');

Route::post('replies/{reply}/favorites', 'FavoritesController@store');
Route::delete('replies/{reply}/favorites', 'FavoritesController@destroy');

Route::get('channels', 'ChannelsController@index');

Route::get('profiles/{user}', 'ProfilesController@show');

Route::get('profiles/{user}/notifications', 'UserNotificationsController@index');
Route::delete('profiles/{user}/notifications/{notification}', 'UserNotificationsController@destroy');

Route::get('users', 'UsersController@index');
Route::get('users/current', 'UsersController@authenticated');
Route::post('users/{user}/avatar', 'UserAvatarController@store');
