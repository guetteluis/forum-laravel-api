<?php

namespace Tests\Unit;

use App\Models\Channel;
use App\Models\Thread;
use App\Models\User;
use App\Notifications\ThreadWasUpdated;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class ThreadTest extends TestCase
{
    use DatabaseMigrations;

    protected $thread;

    public function setUp() {

        parent::setUp();

        $this->thread = create(Thread::class);

    }

    /**
     *
     * @return void
     */
    public function test_a_thread_has_many_replies() {

        $this->assertInstanceOf(Collection::class, $this->thread->replies);

    }

    /**
     *
     * @return void
     */
    public function test_a_thread_has_a_creator() {

        $this->assertInstanceOf(User::class, $this->thread->creator);

    }

    /**
     *
     * @return void
     */
    public function test_a_thread_can_add_a_reply() {

        $this->thread->addReply([
            'body' => 'Foobar',
            'user_id' => 1
        ]);

        $this->assertCount(1, $this->thread->replies);

    }

    public function test_a_thread_notifies_all_subscribers_when_a_reply_is_added() {

        Notification::fake();

        $this->signIn()
            ->thread
            ->subscribe()
            ->addReply([
            'body' => 'Foobar',
            'user_id' => 999
        ]);

        Notification::assertSentTo(auth('api')->user(), ThreadWasUpdated::class);

    }

    public function test_a_thread_belongs_to_a_channel() {

        $this->assertInstanceOf(Channel::class, $this->thread->channel);

    }

    public function test_a_thread_can_be_subscribed_to() {

        $thread = create(Thread::class);

        $thread->subscribe($userId = 1);

        $this->assertEquals(
            1,
            $thread->subscriptions()->where('user_id', $userId)->count()
        );

    }

    public function test_a_thread_can_be_unsubscribed_from() {

        $this->thread->subscribe($userId = 1);

        $this->thread->unsubscribe($userId);

        $this->assertCount(
            0,
            $this->thread->subscriptions
        );

    }

    public function test_it_knows_if_an_authenticated_user_is_subscribed_to_it() {

        $this->signIn();

        $this->thread->subscribe();

        $this->assertTrue($this->thread->subscribed);

        $this->thread->unsubscribe();

        $this->assertFalse($this->thread->subscribed);

    }

    public function test_a_thread_can_read_if_the_authenticated_user_has_read_all_replies() {

        $this->signIn();

        $user= auth('api')->user();

        $this->assertTrue($this->thread->has_updates);

        $user->read($this->thread);

        $this->assertFalse($this->thread->has_updates);
    }
}
