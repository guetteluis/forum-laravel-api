<?php

namespace Tests\Unit;

use App\Models\Activity;
use App\Models\Channel;
use App\Models\Reply;
use App\Models\Thread;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ActivityTest extends TestCase
{
    use DatabaseMigrations;

    public function test_it_records_activity_when_a_thread_is_created() {

        $this->signIn();

        $thread = create(Thread::class);

        $this->assertDatabaseHas('activities', [
            'type' => 'created_thread',
            'user_id' => auth()->id(),
            'subject_id' => $thread->id,
            'subject_type' => 'App\Models\Thread'
        ]);

        $activity = Activity::first();

        $this->assertEquals($activity->subject->id, $thread->id);

    }

    public function test_it_records_activity_when_a_reply_is_created() {

        $this->signIn();

        $reply = create(Reply::class);

        $this->assertEquals(2, Activity::count());

        $this->assertDatabaseHas('activities', [
            'type' => 'created_reply',
            'user_id' => auth()->id(),
            'subject_id' => $reply->id,
            'subject_type' => 'App\Models\Reply'
        ]);

        $activity = Activity::first();

        $this->assertEquals($activity->subject->id, $reply->id);

    }

    public function test_it_fetches_a_feed_for_any_user() {

        $this->signIn();

        create(Thread::class, ['user_id' => auth()->id()], 2);

        auth()->user()->activities()->first()->update(['created_at' => Carbon::now()->subWeek()]);

        $feed = Activity::feed(auth()->user());

        $threads = array_column($feed['items']->toArray(), 'subject');

        $this->assertEquals([2, 1], array_column($threads, 'id'));

    }
}
