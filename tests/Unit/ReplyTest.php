<?php

namespace Tests\Unit;

use App\Models\Reply;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ReplyTest extends TestCase
{
    use DatabaseMigrations;

    public function test_it_has_an_owner(){

        $reply = create(Reply::class);

        $this->assertInstanceOf(User::class, $reply->owner);

    }

    public function test_it_knows_if_it_was_just_published() {

        $reply = create(Reply::class);

        $this->assertTrue($reply->wasJustPublished());

        $reply->created_at = Carbon::now()->subMonth();

        $this->assertFalse($reply->wasJustPublished());

    }

    public function test_it_can_detect_all_mentioned_users_in_the_body() {

        $john = create(User::class, ['name' => 'JohnDoe']);
        $jane = create(User::class, ['name' => 'JaneDoe']);

        $reply = create(Reply::class, [
            'body' => '@JaneDoe wants to talk to @JohnDoe'
        ]);

        $this->assertEquals(
            ['JohnDoe', 'JaneDoe'],
            $reply->mentionedUsers->map(function ($user) {
                return $user->name;
            })->toArray()
        );

    }

    /*public function test_it_shows_mentioned_usernames() {

        $reply = create(Reply::class, [
            'body' => '@JaneDoe wants to talk to @JohnDoe'
        ]);

       dd($reply->fresh());

    }*/
}
