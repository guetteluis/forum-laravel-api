<?php

namespace Tests\Unit;

use App\Models\Channel;
use App\Models\Thread;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ChannelTest extends TestCase
{
    use DatabaseMigrations;

    protected $channel;

    public function setUp() {

        parent::setUp();
        $this->channel = create(Channel::class);

    }

    /**
     *
     * @return void
     */
    public function test_a_channel_has_many_threads() {

        $thread = create(Thread::class, ['channel_id' => $this->channel->id]);

        $this->assertTrue($this->channel->threads->contains($thread));

    }
}
