<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp()
    {
        parent::setUp();
        $this->withoutExceptionHandling();

    }

    protected function signIn($user = null) {

        $user = $user ?: create(User::class);

        Passport::actingAs($user);

        return $this;

    }

}
