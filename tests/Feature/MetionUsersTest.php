<?php

namespace Tests\Feature;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class MetionUsersTest extends TestCase
{

    use DatabaseMigrations;

    public function test_mentioned_users_in_a_reply_are_notified() {

        $john = create(User::class, ['name' => 'JohnDoe']);

        $this->signIn($john);

        $jane = create(User::class, ['name' => 'JaneDoe']);

        $thread = create(Thread::class);

        $reply = raw(Reply::class, ['body' => '@JaneDoe look at this']);

        $this->post("/api/threads/{$thread->channel->slug}/{$thread->id}/replies", $reply);

        $this->assertCount(1, $jane->notifications);

    }

    public function test_it_can_fetch_all_mentioned_users_starting_with_the_given_characters() {

        create(User::class, ['name' => 'johnDoe']);
        create(User::class, ['name' => 'johnDoe2']);
        create(User::class, ['name' => 'janeDoe']);

        $results = $this->json('GET', '/api/users', ['name' => 'john']);

        $this->assertCount(2, $results->json()['users']);

    }

}
