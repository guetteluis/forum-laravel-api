<?php

namespace Tests\Feature;

use App\Models\Activity;
use App\Models\Channel;
use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ManageThreadsTest extends TestCase
{

    use DatabaseMigrations;

    public function test_guests_may_not_create_threads() {

        $this->withExceptionHandling();

        $this->post('/api/threads', [])
            ->assertStatus(401);

    }

    public function test_an_authenticated_user_can_create_new_threads() {

        $this->signIn();

        $thread = make(Thread::class);

        $this->post('/api/threads', $thread->toArray());
        
        $this->assertDatabaseHas('threads', ['title' => $thread->title, 'body' => $thread->body]);

    }

    public function test_a_thread_requires_a_title () {

        $this->withExceptionHandling();

        $this->publishThread(['title' => null])
            ->assertSessionHasErrors('title');

    }

    public function test_a_thread_requires_a_body () {

        $this->withExceptionHandling();

        $this->publishThread(['body' => null])
            ->assertSessionHasErrors('body');

    }

    public function test_a_thread_requires_a_valid_channel () {

        $this->withExceptionHandling();

        $this->publishThread(['channel_id' => null])
            ->assertSessionHasErrors('channel_id');

        $this->publishThread(['channel_id' => 999])
            ->assertSessionHasErrors('channel_id');

    }

    public function test_a_guest_cannot_delete_threads() {

        $this->withExceptionHandling();

        $thread = create(Thread::class);

        $this->json('DELETE', '/api/threads/'. $thread->channel->slug . '/' . $thread->id)
            ->assertStatus(401);

    }

    public function test_unauthorized_users_may_not_delete_threads() {

        $this->withExceptionHandling();

        $thread = create(Thread::class);

        $this->signIn();

        $response = $this->json('DELETE', '/api/threads/'. $thread->channel->slug . '/' . $thread->id);

        $response->assertStatus(403);

        $this->assertDatabaseHas('threads', ['id' => $thread->id]);

    }

    public function test_authorized_users_can_delete_threads () {

        $this->signIn();

        $thread = create(Thread::class, ['user_id' => auth()->id()]);
        $reply = create(Reply::class, ['thread_id' => $thread->id]);

        $response = $this->json('DELETE', '/api/threads/'. $thread->channel->slug . '/' . $thread->id);

        $response->assertStatus(204);

        $this->assertDatabaseMissing('threads', ['id' => $thread->id]);
        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);

        $this->assertEquals(0, Activity::count());

    }

    protected function publishThread($overrides = []) {

        $this->signIn();

        $thread = make(Thread::class, $overrides);

        return $this->post('/api/threads', $thread->toArray());

    }

}
