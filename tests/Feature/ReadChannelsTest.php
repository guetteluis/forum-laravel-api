<?php

namespace Tests\Feature;

use App\Models\Channel;
use App\Models\Reply;
use App\Models\Thread;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ReadChannelsTest extends TestCase
{
    use DatabaseMigrations;

    protected $channel;

    public function setUp()
    {
        parent::setUp();

        $this->channel = create(Channel::class);
    }

    /**
     * all threads test.
     *
     * @return void
     */
    public function test_a_user_can_view_all_channels() {

        $this->get('api/channels')
            ->assertSee($this->channel->name);

    }
}
