<?php

namespace Tests\Feature;

use App\Models\Channel;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ReadUsersTest extends TestCase
{

    use DatabaseMigrations;

    public function test_an_authenticated_user_can_get_his_information() {

        $this->signIn();

        $this->get('api/users/current')
            ->assertSee(auth()->user()->email);

    }

}
