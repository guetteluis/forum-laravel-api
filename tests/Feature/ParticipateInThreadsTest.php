<?php

namespace Tests\Feature;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ParticipateInThreadsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @return void
     */
    public function test_unauthenticated_user_may_not_add_replies() {

        $this->withExceptionHandling();

        $this->post('/api/threads/some-channel/1/replies', [])
            ->assertStatus(401);

    }

    /**
     * @return void
     */
    public function test_an_authenticated_user_may_participate_in_forum_threads() {

        $this->signIn();

        $thread = create(Thread::class);

        $reply = make(Reply::class);

        $this->post('/api/threads/' . $thread->channel->slug . '/' . $thread->id . '/replies', $reply->toArray());

        $this->assertDatabaseHas('replies', ['body' => $reply->body]);
        $this->assertEquals(1, $thread->fresh()->replies_count);

    }

    public function test_a_reply_requires_a_body() {

        $this->withExceptionHandling();

        $this->signIn();

        $thread = create(Thread::class);

        $reply = make(Reply::class, ['body' => null]);

        $this->post('/api/threads/' . $thread->channel->slug . '/' . $thread->id . '/replies', $reply->toArray())
        ->assertSessionHasErrors('body');

    }

    public function test_unauthorized_users_cannot_delete_replies() {

        $this->withExceptionHandling();

        $reply = create(Reply::class);

        $this->delete("/api/replies/{$reply->id}")
           ->assertStatus(401);

        $this->signIn()
            ->delete("/api/replies/{$reply->id}")
            ->assertStatus(403);

    }

    public function test_authorized_users_can_delete_replies() {

        $this->signIn();

        $reply = create(Reply::class, ['user_id' => auth()->guard('api')->id()]);

        $this->delete("/api/replies/{$reply->id}")
            ->assertStatus(200);

        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
        $this->assertEquals(0, $reply->thread->fresh()->replies_count);

    }

    public function test_unauthorized_users_cannot_update_replies() {

        $this->withExceptionHandling();

        $oldBody = 'old body';
        $newBody = 'body has bee changed';

        $reply = create(Reply::class, ['body' => $oldBody]);

        $this->patch("/api/replies/{$reply->id}", ['body' => $newBody])
            ->assertStatus(401);

        $this->signIn();

        $this->patch("/api/replies/{$reply->id}", ['body' => $newBody])
            ->assertStatus(403);

        $this->assertDatabaseHas('replies', [
            'id' => $reply->id,
            'body' => $oldBody
        ]);
    }

    public function test_authorized_users_can_update_replies() {

        $this->withExceptionHandling();

        $this->signIn();

        $reply = create(Reply::class, [
            'user_id' => auth()->guard('api')->id(),
            'body' => 'old body'
        ]);

        $newBody = 'body has been changed';

        $this->patch("/api/replies/{$reply->id}", ['body' => $newBody])
            ->assertStatus(200);

        $this->assertDatabaseHas('replies', [
            'id' => $reply->id,
            'body' => $newBody
        ]);
    }

    public function test_replies_that_contain_spam_may_not_be_created() {

        $this->signIn();

        $thread = create(Thread::class);

        $reply = make(Reply::class, [
            'body' => 'Yahoo Customer Support'
        ]);

        $this->expectException(\Exception::class);

        $this->post("/api/threads/{$thread->channel->slug}/{$thread->id}/replies", $reply->toArray());

    }

    public function test_users_may_only_reply_a_maximum_of_once_per_minute() {

        $this->withExceptionHandling();

        $this->signIn();

        $thread = create(Thread::class);

        $reply = make(Reply::class, [
            'body' => 'Cool Reply'
        ]);

        $this->post("/api/threads/{$thread->channel->slug}/{$thread->id}/replies", $reply->toArray())
            ->assertStatus(200);

        $this->post("/api/threads/{$thread->channel->slug}/{$thread->id}/replies", $reply->toArray())
            ->assertStatus(429);
    }
}
