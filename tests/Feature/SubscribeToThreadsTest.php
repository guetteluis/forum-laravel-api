<?php

namespace Tests\Feature;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Passport\Passport;
use Tests\TestCase;

class SubscribeToThreadsTest extends TestCase
{
    use DatabaseMigrations;

    public function test_a_user_can_subscribe_to_thread() {

        $this->signIn();

        $thread = create(Thread::class);

        $this->post("api/threads/{$thread->channel->slug}/{$thread->id}/subscriptions");

        $this->assertCount(1, $thread->subscriptions);

    }

    public function test_a_user_can_unsubscribe_from_thread() {

        $this->signIn();

        $thread = create(Thread::class);

        $thread->subscribe();

        $this->delete("api/threads/{$thread->channel->slug}/{$thread->id}/subscriptions");

        $this->assertCount(0, $thread->subscriptions);

    }
}
