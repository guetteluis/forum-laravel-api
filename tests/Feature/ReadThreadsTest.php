<?php

namespace Tests\Feature;

use App\Models\Channel;
use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ReadThreadsTest extends TestCase
{
    use DatabaseMigrations;

    protected $thread;

    public function setUp()
    {
        parent::setUp();

        $this->thread = create(Thread::class);
    }

    /**
     * all threads test.
     *
     * @return void
     */
    public function test_a_user_can_view_all_threads() {

        $this->get('api/threads')
            ->assertSee($this->thread->title);

    }

    /**
     * single thread test.
     *
     * @return void
     */
    public function test_a_user_can_read_a_single_thread() {

        $this->get('api/threads/' . $this->thread->channel->slug. '/' . $this->thread->id)
            ->assertSee($this->thread->title);

    }

    public function test_a_user_get_not_found_response_if_try_find_with_invalid_information() {

        $this->get('api/threads/test-channel/999')
            ->assertStatus(404);

    }

    public function test_a_user_can_filter_threads_according_to_a_channel() {

        $channel = create(Channel::class);
        $threadInChannel = create(Thread::class, ['channel_id' => $channel->id]);
        $threadNotInChannel = create(Thread::class);

        $this->get('api/threads/' . $channel->slug)
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);

    }

    public function test_a_user_can_filter_threads_by_any_username() {

        $this->signIn(create(User::class, ['name' => 'JohnDoe']));

        $threadByJohn = create(Thread::class, ['user_id' => auth()->id()]);
        $threadNotByJohn = create(Thread::class);

        $this->get('/api/threads?by=JohnDoe')
            ->assertSee($threadByJohn->title)
            ->assertDontSee($threadNotByJohn->title);


    }

    public function test_a_user_can_filter_threads_by_popularity() {

        $this->withoutExceptionHandling();

        $threadWithTwoReplies = create(Thread::class);
        create(Reply::class, ['thread_id' => $threadWithTwoReplies], 2);

        $threadWithThreeReplies = create(Thread::class);
        create(Reply::class, ['thread_id' => $threadWithThreeReplies], 3);

        $threadWithNoReplies = $this->thread;

        $response = $this->get('/api/threads?popular=1')->json();

        $this->assertEquals([3, 2, 0], array_column($response['threads_data']['items'], 'replies_count'));

    }

    public function test_a_user_can_filter_threads_by_those_that_are_unanswered() {

        $thread = create(Thread::class);
        create(Reply::class, ['thread_id' => $thread->id]);

        $response = $this->get('/api/threads?unanswered=1')->json();

        $this->assertCount(1, $response['threads_data']['items']);

    }

    public function test_a_user_can_request_all_replies_for_a_given_thread() {

        $thread = create(Thread::class);
        create(Reply::class, ['thread_id' => $thread->id], 2);

        $response = $this->get("api/threads/{$thread->channel->slug}/{$thread->id}/replies")->json();

        $this->assertCount(2, $response['replies_data']['items']);

    }
}
