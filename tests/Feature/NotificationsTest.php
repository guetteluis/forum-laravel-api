<?php

namespace Tests\Feature;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Notifications\DatabaseNotification;
use Laravel\Passport\Passport;
use Tests\TestCase;

class NotificationsTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->signIn();

    }

    public function test_a_notification_is_prepared_when_a_subscribed_thread_receives_a_new_reply_that_is_not_by_the_current_user() {

        $thread = create(Thread::class)->subscribe();

        $this->assertCount(0, auth('api')->user()->notifications);

        $thread->addReply([
            'user_id' => auth('api')->id(),
            'body' => 'Subscription test'
        ]);

        $this->assertCount(0, auth('api')->user()->fresh()->notifications);

        $thread->addReply([
            'user_id' => create(User::class)->id,
            'body' => 'Subscription test'
        ]);

        $this->assertCount(1, auth('api')->user()->fresh()->notifications);

    }

    public function test_a_user_can_fetch_unread_notifications() {

        $authUser = auth('api')->user();

        create(DatabaseNotification::class);

        $response = $this->get("api/profiles/{$authUser->name}/notifications")->json();

        $this->assertCount(1, $response['notifications']);

    }

    public function test_a_user_can_mark_a_notification_as_read() {

        $authUser = auth('api')->user();

        create(DatabaseNotification::class);

        $this->assertCount(1, $authUser->unreadNotifications);

        $notificationId = $authUser->unreadNotifications->first()->id;

        $this->delete("api/profiles/{$authUser->name}/notifications/{$notificationId}");

        $this->assertCount(0, $authUser->fresh()->unreadNotifications);

    }
}
