<?php

namespace Tests\Feature;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Passport\Passport;
use Tests\TestCase;

class FavoriteTest extends TestCase
{
    use DatabaseMigrations;

    public function test_guest_cannot_favorite_anything() {

        $this->withoutExceptionHandling();

        $reply = create(Reply::class);

        $this->expectException(AuthenticationException::class);

        $this->post('api/replies/'. $reply->id . '/favorites');

    }

    public function test_authenticated_user_can_favorite_any_reply() {

        $this->signIn();

        $reply = create(Reply::class);

        $this->post('api/replies/'. $reply->id . '/favorites');

        $this->assertCount(1, $reply->favorites);

    }

    public function test_authenticated_user_can_unfavorite_a_reply() {

        $this->signIn();

        $reply = create(Reply::class);

        $reply->favorite();

        $this->delete("api/replies/{$reply->id}/favorites")
            ->assertStatus(200);

        $this->assertCount(0, $reply->fresh()->favorites);

    }

    public function test_authenticated_user_may_only_favorite_a_reply_once() {

        $this->withoutExceptionHandling();

        $this->signIn();

        $reply = create(Reply::class);

        $this->post('api/replies/'. $reply->id . '/favorites');

        $this->assertCount(1, $reply->favorites);

    }

    public function test_authenticated_user_can_check_if_favorited_a_reply() {

        $this->withoutExceptionHandling();

        $this->signIn();

        $thread = create(Thread::class);

        $reply = create(Reply::class, [
            'thread_id' => $thread->id
        ]);

        $this->post('api/replies/'. $reply->id . '/favorites');

        $this->get("api/threads/{$thread->channel->slug}/{$thread->id}/replies")
            ->assertSee('favorited');
    }
}
