<?php

namespace Tests\Feature;

use App\Models\Thread;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ProfilesTest extends TestCase
{

    protected $user;

    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->withoutExceptionHandling();
        $this->user = create(User::class);

    }

    public function test_a_user_has_a_profile() {

        $user = $this->user;

        $this->get("/api/profiles/{$user->name}")
            ->assertSee($user->name);

    }

    public function test_an_authenticated_user_can_see_his_profile() {

        $this->signIn();

        $this->get('api/profiles/' . auth()->user()->name)
            ->assertSee(auth()->user()->email);

    }

    public function test_an_authenticated_user_cannot_see_another_full_profile() {

        $this->signIn();

        $anotherUser = $this->user;

        $this->get('api/profiles/' . $anotherUser->name)
            ->assertDontSee($anotherUser->email);

    }

    public function test_guest_user_cannot_see_another_full_profile() {

        $anotherUser = $this->user;

        $this->get('api/profiles/' . $anotherUser->name)
            ->assertDontSee($anotherUser->email);

    }

    public function test_profiles_display_all_threads_made_by_the_associated_user() {

        $this->signIn($this->user);

        $thread = create(Thread::class, ['user_id' => $this->user->id]);

        $this->get('api/profiles/' . $this->user->name)
            ->assertSee($thread->title)
            ->assertSee($thread->body);

    }

}
